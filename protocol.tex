%!TEX root = main.tex

\section{Protocol Design}
In this section, we will give a more detailed description of protocol design to realize censorship resistance on top of blockchain-based IoT. The key thought is to optimize the part that data transmission is through single point. For simplicity, We assume the set of replicas(full nodes) are denoted as $R$ and $ |R|=3f+1 $, where $f$ is the maximum number of faulty nodes. In addition, the BFT-SMaRt, which implements a modular state machine replication protocol atop a Byzantine consensus algorithm\cite{sousa2012byzantine}, is adopted as the underlying consensus protocol to describe our protocol process. 

\subsection{Dealing with the single entry}
We have discussed the potential censorship risk when data flow from sensor network into blockchain network(we call it as "inbound flow"). Under favourable network conditions and no faulty replicas, figure \ref{fig:in-message-pattern} shows the normal case operation in inbound flow, where following phases are included:
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{in-message-pattern.png}
\caption{\label{fig:in-message-pattern}Message pattern for dealing with the single entry}
\end{figure}
\paragraph{Diffusion phase}
In BFT-SMaRt implementation, it uses TCP socket to achieve message exchange, which may not be suitable for IoT settings since the impossibility of broadcasting via TCP. We introduce gossip protocol, utilized by many systems such as Amazon S3, Usenet NNTP, to achieve message dissemination. Gossip possesses following strengths: 1) Scalable, generally it takes only $O(log(n))$ rounds to reach all nodes, where $n$ is the number of nodes\cite{kermarrec2007gossiping}. 2) Fault-tolerance. They have the ability to operate well in networks with irregular and unknown connectivity\cite{birman2007promise}. 3) Robust. All nodes are equivalent, so failed nodes cannot prevent other nodes from continuing transferring data. 4) Convergent consistency. it achieves exponentially rapid spread of information and, therefore, converge exponentially quickly to a globally consistent after a new event occurs, in the absence of additional events. 5) Extremely decentralized. it provides an extremely decentralized form of information discovery\cite{birman2007promise}. 

In this phase, sensor $s_j$ collects data then disseminates \textit{DIFFUSION} message to all replicas through gossip, triggering the execution of consensus protocol. All replicas keep receiving requests and pack as one batch every $s$ requests, denoted as $b=\langle m_{j1},m_{j2},...,m_{js}\rangle$, where $j$ indicates different types of sensors.

\paragraph{Byzantine consensus phase}
The consensus' leader firstly sends a \textit{PROPOSE} message containing the aforementioned $b$ to other replicas, all other replicas receive the \textit{PROPOSE} message then check the validation of proposed batch and if the sender is the leader, if both are true, then register $b$ and send a \textit{WRITE} message containing a cryptographic hash of the batch, denoted as $H(b)$, to all other replicas. If a replica receives $\lceil \frac{|R|+f+1}{2}\rceil$ \textit{WRITE} messages with identical hash, it sends an \textit{ACCEPT} message containing this hash to all other replicas. If some replicas receive $\lceil \frac{|R|+f+1}{2}\rceil$ \textit{ACCEPT} messages for the same hash,  do the \textit{FILTERING VALIDATION (FV)} by comparing the number of messages $s$ in \textit{PROPOSE} with in \textit{FV} phase so that data loss could be detected. If filtering validation passes, updating the data message to ledger. 

We remark here it may be more efficient to do \textit{FV} after \textit{PROPOSE} phase since the all-to-all communication in \textit{WRITE} and \textit{ACCEPT} phases only relies on hash value, which has nothing to with data message itself. However, it requires extra effort of modifying the original BFT-SMaRt framework, consequently lose the generality when applying other frameworks to our protocol.

This message pattern is executed under the setting that leader is honest and the system is synchronous, if these conditions do not hold, \textit{view change} would happen to elect a new leader and all replicas are required to converge to the same consensus execution. More details are described in \cite{sousa2012byzantine} and we do not describe it in this work.

\subsection{Security analysis}
\textcolor{red}{Editing} protocol on BFT-SMaRt, which implements a modular state machine replication protocol on top of a Byzantine consensus algorithm\cite{sousa2012byzantine}. The \textit{termination}, \textit{agreement} and \textit{validity} are guaranteed by such protocol. For \textit{Integrity}: 
\begin{enumerate}
\item \textbf{Sensor network to BC network}We propose to utilize gossip to realize point-to-point data dissemination instead of UNIX socket, such change theoretically improves the robustness and avoids data loss compared with passing through single gateway node.
\item \textbf{Inside BC network}Filtering validation is done before updating ledger so as to prevent from censorship during the consensus.
\item \textbf{BC network to sensor network}Considering the limited resource and computing power of IoT devices, only consensus' leader involves the communication, re-sending mechanism is also introduced if the leader act maliciously, these steps in response round ensures that the command from the BC network is the consensus of all replicas rather than a single one.
\end{enumerate}

\subsection{Dealing with a single exit}
\textcolor{red}{the start part need to be polised a little bit} outbound means the data flow out of BC network. In IoT scenario, we need to make sure the final decision is the consensus of all participants rather than a single node, figure 3 shows the outbound message pattern.
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{out-message-pattern.png}
\caption{\label{fig:out-message-pattern}Message pattern for dealing with a single exit}
\end{figure}

\paragraph{Decision consensus phase}
After the data batch is written to ledger, each honest node would execute the analytic program with model $y=\mathcal{F}(m_{j1},m_{j2},...,m_{js})$, we assume the program $\mathcal{F}$ is already known as it is application-specific and may vary in different scenarios in practice. The decision consensus phase executes the same steps as aforementioned byzantine consensus phase, the only difference is the content to be agreed, specifically output $y$ instead of data batch ${b}$. We split this two rounds for different consensus content as in some cases, only the data need to be recorded to ledger, while others may need both. At the end of the decision consensus phase, the output based on sensor data message is updated to ledger as well. Then each peer sign the output and send to leader, thus triggering the execution of \textit{RESPONSE} phase.

\paragraph{Response phase}
The response phase includes prepare, confirmation and optional re-confirmation steps. In \textit{RESPONSE PREPARE}, the honest consensus' leader will forward command/instruction $y$ if collecting sufficient signature on the same $y$ to actuators, once actuator receives the instruction and verify its signature, then entering into \textit{RESPONSE CONFIRMATION} phase, in which actuator simply broadcast the signed acknowledgement $ack(c)_\sigma$ to all replicas, other non-leader replicas are waiting for the $ack$ after updating ${y}$ to ledger, if they do not receive the acknowledgement within a pre-defined time period, replicas will all send the $y$ to client.

\subsection{Reducing communication via aggregate signatures}
Data collected from sensors is signed and sent out constantly, to improve the efficiency of execution, every $s$ same type of data $\langle m_{j1},m_{j2},...,m_{js}\rangle$is packed as a batch, then the problem of how to achieve efficient batch data signing and verification need to be solved. Referring to \cite{cryptoeprint:2018:483}, we integrate the modified BLS signature aggregation scheme to our protocol. The scheme utilize following ingredients:
\begin{itemize}
\item A bilinear pairing: $e:G_0\times G_1 \rightarrow G_T$. The pairing is efficiently computable, non-degenerate. All the three groups have prime order $q$. We let $g_0$ and $g_1$ be the generator of $G_0$ and $G_1$ respectively.
\item A hash function $H_0:\mathcal{M}\rightarrow G_0$.  
\item Another hash function $H_1:G_1^n\rightarrow R^n$ where $R:={\{1,2,...,2^{128}\}}$ and where $1\leq n \leq \tilde{N}$. The security analysis will treat both $H_0$ and $H_1$ as random oracles.
\end{itemize}
With these ingredients, the scheme works as follows:
\begin{itemize}
\item $KenGen():$ Choose a random $\alpha \stackrel{R}{\longleftarrow} Z_q$ and set $h \leftarrow g_1^\alpha$. Output $pk:=(h)$ and $sk:=(\alpha)$.
\item $Sign(sk,m_j)$: Sign the message $m_j$ and output $\sigma_j \leftarrow H_0(m_j)^\alpha \in G_0$, where $j=\{1,2,...\}$ indicates data type, namely distinct kind of sensors.
\item $Aggregate((pk_1,\sigma_{j1}),...,(pk_n,\sigma_{jn}))$:
\begin{enumerate}
\item Compute $(t_1,...,t_n) \leftarrow H_1(pk_1,...,pk_n) \in R^n$.
\item Output the multi-signature $\sigma_j \leftarrow \sigma_{j1}^{t_1}\cdot\cdot\cdot \sigma_{jn}^{t_n} \in G_0$.
\end{enumerate}
\item $Verify(pk_1,...,pk_n,m_j,\sigma_j):$ To verify a multi-signature $\sigma_j$ on $m_j$ do 
\begin{enumerate}
\item Compute $(t_1,...,t_n) \leftarrow H_1(pk_1,...,pk_n) \in R^n$. 
\item Compute the aggregate public key $apk \leftarrow pk_1^{t_1}\cdot\cdot\cdot pk_n^{t_n} \in G_1$.
\item If $e(g_1, \sigma_j)=e(apk, H_0(m_j))$ output "accept", otherwise output "reject".
\item Verification always requires two pairings, independent of $n$.
\end{enumerate}
\end{itemize}
\textbf{Batch verification.} In our protocol, we assume every $s$ messages are packed as a batch, so when we can quickly verify the batch rather than verifying them one by one. Suppose we are given triples $(m_{ji}, \sigma_{ji}, apk_i)$ for $i=1,2,...s$, $apk_i$ is the aggregated public key used to verify the multi-signature $\sigma_i$ on $m_i$. We assume the messages $m_1,...,m_s$ are different, so we can verify all these triples as a batch:
\begin{enumerate}
\item Compute an aggregate signature $\tilde{\sigma_j}=\sigma_{j1}\cdot\cdot\cdot\sigma_{js} \in G_1$.
\item Accept all $s$ multi-signature tuples as valid iff $e(g_1,\tilde{\sigma_j})=e(apk_1,H_0(m_1)\cdot\cdot\cdot e(apk_s,H_0(m_s)))$.
\end{enumerate}
This way, verifying the $s$ multi-signatures requires only $s+1$ pairings instead of $2s$ pairings to verify them one by one. Besides BFT-SMaRt's step in protocol, others steps could apply such multi-signature aggregation scheme so as to improve verification efficiency considerably. 