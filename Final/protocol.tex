%!TEX root = main.tex

\section{Protocol Design}
In this section, we will give a more detailed description of protocol design to realize censorship resistance on top of blockchain-based IoT. The key thought is to optimize the part that data transmission is through single point. For simplicity, We assume the set of replicas(full nodes) are denoted as $R$ and $ |R|=3f+1 $, where $f$ is the maximum number of faulty nodes. In addition, the BFT-SMaRt, which implements a modular state machine replication protocol atop a Byzantine consensus algorithm\cite{sousa2012byzantine}, is adopted as the underlying consensus protocol to describe our protocol process. 

\subsection{Dealing with the single entry}
We have discussed the potential censorship risk when data flow from sensor network into blockchain network(we call it as "inbound flow"). Under favourable network conditions and no faulty replicas, figure \ref{fig:in-message-pattern} shows the normal case operation in inbound flow, where following phases are included:
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{in-message-pattern.png}
\caption{\label{fig:in-message-pattern}Message pattern for dealing with the single entry}
\end{figure}
\paragraph{Diffusion phase}
In some consensus protocol, like BFT-SMaRt, it uses TCP socket to achieve message exchange, which may not be suitable for IoT settings since the impossibility of broadcasting via TCP. We introduce gossip protocol, utilized by many systems such as Amazon S3, Usenet NNTP, to achieve message dissemination. Gossip possesses following strengths: 1) Scalable, generally it takes only $O(log(n))$ rounds to reach all nodes, where $n$ is the number of nodes\cite{kermarrec2007gossiping}. 2) Fault-tolerance. They have the ability to operate well in networks with irregular and unknown connectivity\cite{birman2007promise}. 3) Robust. All nodes are equivalent, so failed nodes cannot prevent other nodes from continuing transferring data. 4) Convergent consistency. it achieves exponentially rapid spread of information and, therefore, converge exponentially quickly to a globally consistent after a new event occurs, in the absence of additional events. 5) Extremely decentralized. it provides an extremely decentralized form of information discovery\cite{birman2007promise}. 

In this phase, sensor $s_j$ collects data then disseminates \textit{DIFFUSION} message to all replicas through gossip, triggering the execution of consensus protocol. All replicas keep receiving requests and pack as one batch every $s$ requests, denoted as $b=\langle m_{j1},m_{j2},...,m_{js}\rangle$, where $j$ indicates different sensors of the same type.

\paragraph{Byzantine consensus phase}
The consensus' leader firstly sends a \textit{PROPOSE} message containing the aforementioned $b$ to other replicas, all other replicas receive the \textit{PROPOSE} message then check the validation of proposed batch and if the sender is the leader, if both are true, then register $b$ and send a \textit{WRITE} message containing a cryptographic hash of the batch, denoted as $H(b)$, to all other replicas. If a replica receives $\lceil \frac{|R|+f+1}{2}\rceil$ \textit{WRITE} messages with identical hash, it sends an \textit{ACCEPT} message containing this hash to all other replicas. If some replicas receive $\lceil \frac{|R|+f+1}{2}\rceil$ \textit{ACCEPT} messages for the same hash,  do the \textit{FILTERING VALIDATION (FV)} by comparing the number of messages $s$ in \textit{PROPOSE} with in \textit{FV} phase so that data loss could be detected. If filtering validation passes, updating the data message to ledger. 

We remark here it may be more efficient to do \textit{FV} after \textit{PROPOSE} phase since the all-to-all communication in \textit{WRITE} and \textit{ACCEPT} phases only relies on hash value, which has nothing to with data message itself. However, it requires extra effort of modifying the original BFT-SMaRt framework, consequently lose the generality when applying other frameworks to our protocol.

This message pattern is executed under the setting that leader is honest and the system is synchronous, if these conditions do not hold, \textit{view change} would happen to elect a new leader and all replicas are required to converge to the same consensus execution. More details are described in \cite{sousa2012byzantine} and we do not describe it in this work.

\subsection{Dealing with a single exit}
Within the blockchain-based IoT scenario, we need to make sure the instruction from blockchain network to sensor network (called "outbound flow") is the consensus of most participants rather than a single node, otherwise that single node could be hacked and block some emergent instruction thus leading to serious result. Fig. \ref{fig:out-message-pattern} shows the normal case operation in outbound flow, which contains following phases:
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{out-message-pattern.png}
\caption{\label{fig:out-message-pattern}Message pattern for dealing with a single exit}
\end{figure}

\paragraph{Decision consensus phase}
After the data batch is written to ledger, each honest node would execute the analytic program with model $y=\mathcal{F}(m_{j1},m_{j2},...,m_{js})$, we assume the program $\mathcal{F}$ is already known as it is application-specific and may vary in different scenarios in practice. The decision consensus phase executes the same steps as aforementioned byzantine consensus phase, the only difference is the content to be agreed, specifically output $y$ instead of data batch ${b}$. We split this two rounds for different consensus content as in some cases, only the data need to be recorded to ledger, while others may need both. At the end of the decision consensus phase, the output based on sensor data message is updated to ledger as well. Then each peer sign the output and send to leader, thus triggering the execution of \textit{RESPONSE} phase.

\paragraph{Response phase}
The response phase includes prepare, confirmation and optional re-confirmation steps. In \textit{RESPONSE PREPARE}, the honest consensus' leader will forward command/instruction $y$ if collecting sufficient signature on the same $y$ to actuators, once actuator receives the instruction and verify its signature, then entering into \textit{RESPONSE CONFIRMATION} phase, in which actuator simply broadcast the signed acknowledgement $ack(c)_\sigma$ to all replicas, other non-leader replicas are waiting for the $ack$ after updating ${y}$ to ledger, if they do not receive the acknowledgement within a pre-defined time period, replicas will all send the $y$ to client.

\subsection{Security analysis}
For inbound flow, namely data flow from sensor network to blockchain network, instead of relying on the gateway node to relay data message, gossip protocol could ensure that message being disseminated to all nodes, thus preventing censorship from the single relay point. Besides, the ensuing filtering validation phase added to the regular BFT consensus protocol checks data loss before updating the state world in blockchain network.

For the outbound flow, i.e. data flow from blockchain network to sensor network, rather than sending instructions to actuators by one single server node, the outcome is determined and endorsed by at least the majority of servers, thus even one single server is blocked, the correctness of the outcome is still guaranteed. In a nutshell, aforementioned two aspects of improvement ensures the security of data flow within blockchain-based IoT system.

\subsection{Reducing communication via aggregate signatures}
Data collected from sensors is signed and sent out constantly, to improve the efficiency of execution, every $s$ same type of data $\langle m_{j1},m_{j2},...,m_{js}\rangle$is packed as a batch, normally we need to verify the $s$ messages one by one, which results in a lot of communication. Referring to \cite{cryptoeprint:2018:483}, we propose to leverage the BLS signature aggregation scheme to reduce the communication in our protocol. The scheme utilize following ingredients:
\begin{itemize}
\item A bilinear pairing: $e:G_0\times G_1 \rightarrow G_T$. The pairing is efficiently computable, non-degenerate. All the three groups have prime order $q$. We let $g_0$ and $g_1$ be the generator of $G_0$ and $G_1$ respectively.
\item A hash function $H_0:\mathcal{M}\rightarrow G_0$.  This hash function will be treated as a random oracle in the security analysis.
\end{itemize}
With these ingredients, the scheme works as follows:
\begin{itemize}
\item $KenGen():$ Choose a random $\alpha \stackrel{R}{\longleftarrow} Z_q$ and set $h \leftarrow g_1^\alpha \in G_1$, output $pk:=(h)$ and $sk:=(\alpha)$.
\item $Sign(sk,m_j)$: Sign the message $m_j$ and output $\sigma_j \leftarrow H_0(m_j)^\alpha \in G_0$, where $j=\{1,2,...\}$ indicates different sensors of the same type. The signature is a \textit{single} group element.
\item $Verify(pk,m_j,\sigma_j):$ If $e(g_1, \sigma_j)=e(pk,H_0(m_j))$ output "accept", otherwise output "reject".
\end{itemize}
\textbf{Signature aggregation.} In our protocol, we assume every $s$ messages are packed as a batch, so when we can quickly verify the batch rather than verifying them one by one. given triples $(pk_i,m_{ij},\sigma_{ij})$ for $i=1,2,...s$,  we can aggregate the signatures $\sigma_{1j}, \sigma_{2j}, \cdot\cdot\cdot, \sigma_{sj} \in G_0$into a short convincing aggregate signature $\tilde{\sigma_j} \leftarrow\sigma_{1j}\cdot\cdot\cdot\sigma_{sj} \in G_0$. Then verifying an aggregate signature $\tilde{\sigma_j} \in G_0$ is done by checking that $$e(g_1, \tilde{\sigma_j})=e(pk_1,H_0(m_{1j})) \cdot\cdot\cdot e(pk_s,H_0(m_{sj}))$$Generally the messages $m_{1j},...,m_{sj}$ are different since the data collected from distinct sensors of the same type at varied timestamp. This way, verifying the $s$ signatures requires $s+1$ pairings instead of $2s$ pairings to verify them one by one. Such scheme can be used in several phases in protocol. Specifically, when packing the $s$ messages to one block, the communication between sensors and servers is  relatively declined. By the same token, when honest consensus' leader forwards instructions that signed by most server nodes to actuator, the verification speed could be comparatively improved.